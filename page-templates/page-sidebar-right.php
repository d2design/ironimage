<?php
/*
Template Name: Right Sidebar
*/
get_header(); ?>

	<div class="main-wrap sidebar-right" role="main">

		<?php do_action( 'foundationpress_before_content' );
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/page-content' );
		endwhile;

		do_action( 'foundationpress_after_content' );
		get_sidebar(); ?>

	</div>

<?php get_footer();
