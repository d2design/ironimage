<?php
/*
Template Name: Full Width
*/
get_header(); ?>
		<div class="main-wrap full-width" role="main">
			<?php do_action( 'foundationpress_before_content' ); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/page-content' ); ?>
			<?php endwhile; ?>

			<?php do_action( 'foundationpress_after_content' ); ?>
		</div>
<?php get_footer();
