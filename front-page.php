<?php
/*
Template Name: Front Page
*/
get_header();

do_action( 'before_after_content' );

?>
	<section class="services">
		<div class="row">
			<div class="small-12 large-12 columns">
				<header>
					<h2 class="text-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/iif_kettlebell.svg"
							 alt="Kettlbell" aria-hidden="true">
								<?php esc_html_e( 'Services', 'foundationpress' ); ?>
					</h2>
				</header>
			</div>
		</div>

		<?php

		echo '<div class="row small-up-1 medium-up-4 mt-20">';

		$i = 1; // Count to return a new 4-col row

		$args = array(
			'post_type'     => 'page',
			'category_name' => 'services',
		);

		$query = new WP_Query ( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post(); ?>

				<div class="column">
					<div class="card align-stretch">
						<?php the_post_thumbnail( 'featured-small' ); ?>
						<div class="card-section flex-card-example">
							<h4><a href="<?php the_permalink(); ?>" rel="bookmark"
								   title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
							<?php the_excerpt(); ?>
							<a class="button secondary expanded" href="<?php the_permalink(); ?>">More Details</a>
						</div>
					</div>
				</div>

				<?php
				// After 4 close the row div and open a new one
				if ( $i % 4 == 0 ) {
					echo '</div><div class="row small-up-1 medium-up-4">';
				}
			}
		}
		?>

		<?php wp_reset_postdata(); ?>
	</section>

	<section class="background-primary mb-80">
		<div class="row pt-40 pb-40">
			<div class="small-12 large-12 columns">
				<header>
					<h4>
						<em class="text-white">
							<?php dynamic_sidebar( 'testimonial-widget' ); ?>
						</em></h4>
				</header>
			</div>
		</div>
	</section>

<?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		// Your loop code
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>

<?php do_action( 'foundationpress_after_content' ); ?>

<?php get_footer();
