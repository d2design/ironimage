<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since   FoundationPress 1.0.0
 */

get_header(); ?>

	<div class="main-wrap" role="main">
		<?php do_action( 'foundationpress_before_content' ); ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/page-content' ); ?>
			<div class="small-12 medium-12 large-4 columns">
				<?php get_sidebar(); ?>
			</div>

		<?php endwhile; ?>

		<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
<?php get_footer();
