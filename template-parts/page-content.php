<?php
/**
 * Page content template
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since   FoundationPress 1.0.0
 */

?>
<article <?php post_class( 'main-content' ) ?> id="post-<?php the_ID(); ?>">
	<header class="background-dark-gray">
		<h1 class="entry-title text-white mr-20 ml-20"><?php the_title(); ?></h1>
		<?php echo the_post_thumbnail( 'featured-large' ); ?>
	</header>
	<?php do_action( 'foundationpress_page_before_entry_content' ); ?>

	<div class="columns">
		<?php the_content(); ?>
	</div>
</article>