<?php
/*
 * Front Page Hero section.
 *
 * */
?>

<header class="front-hero" style="background: url('<?php echo get_theme_mod( 'hero_image', 'fp-xlarge' ); ?>') center no-repeat; background-size: cover;">
	<div class="marketing">
		<div class="tagline text-white">
			<?php echo get_theme_mod( 'hero_quote' ); ?>
		</div>
    </div>
</header>