jQuery(document).foundation();


 $(function() {
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $(".top-bar a").each(function() {
            // checks if its the same on the address bar
            if (url == (this.href)) {
                $(this).closest("li").addClass("underline");
                //for making parent of submenu active
               $(this).closest("li").parent().parent().addClass("active");
            }
        });
    });

//Navbar Box Shadow on Scroll
$( function() {
	var navbar = $( '.top-bar' );
	$( window ).scroll( function() {
		if ( $( window ).scrollTop() <= 40 ) {
			navbar.css( 'box-shadow', 'none' );
		} else {
			navbar.css( 'box-shadow', '0px 10px 20px rgba(0, 0, 0, 0.4)' );
		}
	} );
} );