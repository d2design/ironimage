<?php if ( has_post_thumbnail() ) {
	
	// Get the post thumbnail URL
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

} else {
	
	// Get the default featured image in theme options
	$feat_image = get_field('default_featured_image', 'option');

} ?>