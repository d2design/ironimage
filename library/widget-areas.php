<?php
/**
 * Register widget areas
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_sidebar_widgets' ) ) :
function foundationpress_sidebar_widgets() {
	register_sidebar(array(
		'id' => 'sidebar-widgets',
		'name' => __( 'Sidebar widgets', 'foundationpress' ),
		'description' => __( 'Drag widgets to this sidebar container.', 'foundationpress' ),
		'before_widget' => '<article id="%1$s" class="widget %2$s">',
		'after_widget' => '</article>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'navbar-widget',
		'name' => __( 'Navbar widgets', 'foundationpress' ),
		'description' => __( 'Drag widgets to this navbar container', 'foundationpress' ),
		'before_widget' => '<div class="title-bar-left ecommerce-header-top-message mt-10">',
		'after_widget' => '</div>',
		'before_title' => '<p>',
		'after_title' => '</p>',
	));

	register_sidebar( array(
		'id'            => 'testimonial-widget',
		'name'          => __( 'Testimonial widgets', 'foundationpress' ),
		'description'   => __( 'Drag widgets to this Testimonial container', 'foundationpress' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar(array(
		'id' => 'footer-widgets',
		'name' => __( 'Footer widgets', 'foundationpress' ),
		'description' => __( 'Drag widgets to this footer container', 'foundationpress' ),
		'before_widget' => '<article id="%1$s" class="large-4 columns widget %2$s">',
		'after_widget' => '</article>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
}

add_action( 'widgets_init', 'foundationpress_sidebar_widgets' );
endif;
