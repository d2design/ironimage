<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 */

function foundationpress_customizer( $wp_customize ) {

	$wp_customize->add_section( 'hero', array(
		'title'       => __( 'Hero', 'foundationpress' ),
		'priority'    => 1,
		'description' => __( 'Hero Image and Quote.' ) // optional
	) );

	$wp_customize->add_setting( 'hero_image', array(
		'default'   => 'https://via.placeholder.com/1080x750',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'hero_image',
			array(
				'label'    => __( 'Image', 'foundationpress' ),
				'section'  => 'hero',
				'settings' => 'hero_image',
				'context'  => 'hero-image',
			)
		) );

	function customizer_textarea_sanitizer( $text ) {
		return esc_textarea( $text );
	}

	$wp_customize->add_setting( 'hero_quote', array(
		'default'   => __( 'Quote message', 'foundationpress' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_textarea_sanitizer',
	) );

	$wp_customize->add_control( 'hero_quote', array(
		'label'    => __( 'Hero Quote', 'foundationpress' ),
		'section'  => 'hero',
		'settings' => 'hero_quote',
		'type'     => 'textarea',
	) );
}



add_action( 'customize_register', 'foundationpress_customizer' );