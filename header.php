<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since   FoundationPress 1.0.0
 */
?>
	<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
<?php do_action( 'foundationpress_after_body' ); ?>

<?php do_action( 'foundationpress_layout_start' ); ?>

	<header class="site-header nav-menu-up" role="banner">
		<div class="top-bar-container" data-sticky-container>
			<div class="title-bar-container sticky" data-sticky
				 data-options="anchor: anchor-top; marginTop: 0; stickyOn: small;" style="width:100%; z-index:2">
				<!-- NOTE: This is the header menu that appears at the top of your site. -->
				<div class="off-canvas-content" data-off-canvas-content>
					<div class="title-bar">

						<?php dynamic_sidebar( 'navbar-widget' ); ?>

						<div class="title-bar-right show-for-small-only">
							Menu
							<button class="menu-icon" type="button" data-toggle="offcanvas-full-screen"><span aria-hidden
								="true"></span></button>
						</div>
						<div class="ecommerce-header-top-links background-black">
							<?php foundationpress_top_bar_r(); ?>
						</div>
					</div>

					<nav class="site-navigation top-bar background-light-gray" role="navigation">
						<div class="top-bar-left">
							<div class="site-desktop-title top-bar-title">
								<a href="<?php echo home_url(); ?>">
									<img class="logo mb20" src="<?php bloginfo( 'template_directory' ); ?>/assets/images/iron-image-fitness-logo.svg"><span
										class="site-name"><?php bloginfo( 'name' ); ?></span></a>
							</div>
						</div>

						<div class="top-bar-right mt-20">
							<?php foundationpress_top_bar_c(); ?>
						</div>

					</nav>
				</div>
			</div>
		</div>

		<!-- MOBILE MENU -->
		<div class="mobile-off-canvas-menu">
			<div id="offcanvas-full-screen" class="offcanvas-full-screen" data-off-canvas data-transition="overlap">
				<div class="offcanvas-full-screen-inner">
					<button class="offcanvas-full-screen-close" aria-label="Close menu" type="button" data-close>
						<span aria-hidden="true">&times</span>
					</button>

					<ul class="offcanvas-full-screen-menu">
						<?php foundationpress_mobile_nav() ?>
					</ul>
				</div>
			</div>
		</div>
	</header>

<?php
if ( is_front_page() && is_home() ) {
	get_template_part( 'template-parts/hero' );
}
?>
	<section class="container mt-60">
<?php do_action( 'foundationpress_after_header' );